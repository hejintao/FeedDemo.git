//
//  SDWebASDKImageContainer.h
//  WebASDKImageManager
//
//  Created by Scott Kensell on 2/20/17.
//  Copyright © 2017 James Ide. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Texture/AsyncDisplayKit/AsyncDisplayKit.h>

@interface SDWebASDKImageContainer : NSObject <ASImageContainerProtocol>

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSData *imageData;

- (instancetype)initWithImage:(UIImage *)image data:(NSData *)data;
+ (instancetype)containerForImage:(UIImage *)image;
+ (instancetype)containerForImage:(UIImage *)image data:(NSData *)data;

@end
