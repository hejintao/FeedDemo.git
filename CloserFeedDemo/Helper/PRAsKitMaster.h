//
//  PRAsKitMaster.h
//  CloserHomePage
//
//  Created by 何锦涛 on 2018/6/7.
//  Copyright © 2018年 hither. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PRAsKitMaster : NSObject

/**  Node富文本  */
+ (NSAttributedString *)nodeAttributesStringText:(NSString *)text
                                       TextColor:(UIColor *)textColor
                                            Font:(UIFont *)font;

/**  nodeLabel */
+ (ASTextNode *)nodeTextNodeAddNode:(ASDisplayNode *)addNode;

/**  nodeView */
+ (ASDisplayNode *)nodeDisplayNodeAddNode:(ASDisplayNode *)addNode
                          BackgroundColor:(UIColor *)backgroundColor;

/**  nodeButton 纯文本 */
+ (ASButtonNode *)nodeButtonNodeAddNode:(ASDisplayNode *)addNode
                                  Title:(NSString *)title
                             TitleColor:(UIColor *)titleColor
                                   Font:(UIFont *)font
                           CornerRadius:(CGFloat)cornerRadius
                        BackgroundColor:(UIColor *)backgroundColor
               ContentVerticalAlignment:(ASVerticalAlignment)contentVerticalAlignment
             ContentHorizontalAlignment:(ASHorizontalAlignment)contentHorizontalAlignment;

/**  nodeButton 图文 */
+ (ASButtonNode *)nodeButtonNodeAddNode:(ASDisplayNode *)addNode
                                  Title:(NSString *)title
                             TitleColor:(UIColor *)titleColor
                                   Font:(UIFont *)font
                                  Image:(UIImage *)image
                         ImageAlignment:(ASButtonNodeImageAlignment)imageAlignment
                           CornerRadius:(CGFloat)cornerRadius
                        BackgroundColor:(UIColor *)backgroundColor
               ContentVerticalAlignment:(ASVerticalAlignment)contentVerticalAlignment
             ContentHorizontalAlignment:(ASHorizontalAlignment)contentHorizontalAlignment;

/**  NodeImageView  本地图片 */
+ (ASImageNode *)nodeImageNodeAddNode:(ASDisplayNode *)addNode
                        ClipsToBounds:(BOOL)clipsToBounds
                          ContentMode:(UIViewContentMode)contentMode;

/**  NodeImageView  网络图片 */
+ (ASNetworkImageNode *)nodeNetworkImageNodeAddNode:(ASDisplayNode *)addNode
                                      ClipsToBounds:(BOOL)clipsToBounds
                                        ContentMode:(UIViewContentMode)contentMode
                                       DefaultImage:(UIImage *)defaultImage;

@end
