//
//  PRStatusHelper.h
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PRStatusHelper : NSObject
/**
 计算单行字体的高度

 @param font 字体的大小
 @return 文字的高度
 */
+ (CGFloat)oneLineTextHeightWithFont:(CGFloat)font;


/** 获得文字宽度  */
+ (CGFloat)oneLineTextWidthWithFont:(CGFloat)font text:(NSString *)text;

/**
 获取资源文件

 @param name 文件名字
 @return 资源文件
 */
+ (NSData *)WBDataWithSourceName:(NSString *)name;

/**
 生成富文本

 @param text 文字
 @param font 字体大小
 @param color 字体颜色
 @return 富文本
 */
+ (NSAttributedString *)attributedStringWithText:(NSString *)text textFont:(CGFloat)font textColor:(UIColor *)color;


/**
 获取富文本的高度

 @param attributeText 富文本
 @param maxWidth 最大的宽度
 @return 富文本的高度
 */
+ (CGFloat)heightForAttributeText:(NSAttributedString *)attributeText maxWidth:(CGFloat)maxWidth;

/** 配置文字居中显示、文字颜色、字体大小  */
+(NSMutableAttributedString *)configWordAlignment:(NSTextAlignment)alignment color:(UIColor *)color font:(UIFont *)font str:(NSString *)str;

+ (CGFloat)oneLineImageHeight:(NSArray *)images;

+ (CGFloat)heightForImages:(NSArray *)images;

@end
