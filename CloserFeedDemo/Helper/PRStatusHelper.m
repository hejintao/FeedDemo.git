//
//  PRStatusHelper.h
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import "PRStatusHelper.h"

@implementation PRStatusHelper
+ (CGFloat)oneLineTextHeightWithFont:(CGFloat)font {
    NSString *temp = @" ";
    CGRect stringRect = [temp boundingRectWithSize:CGSizeMake(1000, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]} context:nil];
    return stringRect.size.height;
}

+(CGFloat)oneLineTextWidthWithFont:(CGFloat)font text:(NSString *)text{
    
    CGRect stringRect = [text boundingRectWithSize:CGSizeMake(1000, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]} context:nil];
    return stringRect.size.width;
}

+ (NSData *)WBDataWithSourceName:(NSString *)name {
    NSString *path = [[NSBundle mainBundle]pathForResource:name ofType:@""];
    if (path == nil) {
        return nil;
    }else {
        return [NSData dataWithContentsOfFile:path];
    }
}

+ (NSAttributedString *)attributedStringWithText:(NSString *)text textFont:(CGFloat)font textColor:(UIColor *)color {
    NSDictionary *attributes = @{
                                 NSFontAttributeName : TextFont(font),
                                 NSForegroundColorAttributeName : color,
                                 NSBaselineOffsetAttributeName : @0
                                 };
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
    return attributedString;
}

+ (CGFloat)heightForAttributeText:(NSAttributedString *)attributeText maxWidth:(CGFloat)maxWidth {
    YYTextContainer *container = [YYTextContainer new];
    container.size = CGSizeMake(maxWidth, CGFLOAT_MAX);
    container.maximumNumberOfRows = 7;
    YYTextLayout *layout = [YYTextLayout layoutWithContainer:container text:attributeText];
    return layout.textBoundingSize.height;
}


/** 配置文字居中显示、文字颜色、字体大小  */
+(NSMutableAttributedString *)configWordAlignment:(NSTextAlignment)alignment color:(UIColor *)color font:(UIFont *)font str:(NSString *)str{
    
    if (str == nil) return nil;
    
    NSMutableAttributedString *mStr = [[NSMutableAttributedString alloc]initWithString:str];
    //颜色
    [mStr addAttribute:NSForegroundColorAttributeName value:color range:str.rangeOfAll];
    //字体大小
    [mStr addAttribute:NSFontAttributeName value:font range:str.rangeOfAll];
    //字体对齐方式
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.alignment = alignment;
    
    [mStr addAttribute:NSParagraphStyleAttributeName value:paragraph range:str.rangeOfAll];
    
    return mStr;
}


/** 图片容器高度  */
+ (CGFloat)heightForImages:(NSArray *)images {
    if(images.count == 0) return 0;
    
    if(images.count == 1){
        return 424 * (kScreenWidth-20 * 2)/635.0;
        
    }else if(images.count == 2 || images.count == 4){
        CGFloat oneLineHeight = (kScreenWidth - 5 - 20 * 2) / 2;
        return oneLineHeight*(images.count/2)+((images.count/2)-1)*5; // 图片高度*行数 + 中间间距
        
    }else if(images.count == 3){
        CGFloat oneLineHeight = (kScreenWidth - 5 - 20 * 2) / 2;
        return oneLineHeight*2+5;
        
    }else{
        CGFloat oneLineHeight = (kScreenWidth - 5 * 2 - 20 * 2)/3;
        NSUInteger lines = images.count/3 + (images.count%3>0?1:0);
        return oneLineHeight * lines + 5 * (lines - 1);
    }
    
}

/** 一行图片高度  */
+ (CGFloat)oneLineImageHeight:(NSArray *)images {
    
    if (images.count < 5) {
        if(images.count == 1){
            return kScreenWidth-20 * 2;
            
        }else if(images.count == 2 || images.count == 4 || images.count == 3){
            return (kScreenWidth - 5 - 20 * 2) / 2;
            
        }else{
            return (kScreenWidth - 20 * (images.count - 1)-5 * 2)/images.count;
        }
    }else{
        return (kScreenWidth - 20 * 2 - 5 * 2)/3;
    }
}

@end
