//
//  PRFeedDataModel.m
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import "PRFeedDataModel.h"

@implementation PRFeedDataModel

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"statuses" : [WBStatusModel class]};
}

@end


// ----------------------------------------------
@implementation WBStatusModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"url_struct" : [WBUrlStructModel class],
             @"pic_infos" : [WBPicModel class],
             @"pic_ids" : [NSString class]
             };
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dict {
    _picModelArray = nil;
    if (_pic_ids.count > 0) {
        NSMutableArray *picModelArray = [NSMutableArray array];
        for (NSString *key in _pic_ids) {
            WBPicModel *model = _pic_infos[key];
            [picModelArray addObject:model];
        }
        _picModelArray = [picModelArray copy];
    }
    
    return YES;
}
@end

// -----------------------user model-----------------------
// ----------------------------------------------
// ----------------------------------------------
@implementation WBUserModel

@end

// -----------------------匹配链接url-----------------------
// ----------------------------------------------
// ----------------------------------------------
@implementation WBUrlStructModel

@end

// -----------------------图片数据的model-----------------------
// ----------------------------------------------
// ----------------------------------------------
@implementation WBPicModel

@end

// -----------------------每个图片的元数据-----------------------
// ----------------------------------------------
// ----------------------------------------------
@implementation WBPictureMetaData

@end

// -----------------------pageinfo 区分文章视频啥的-----------------------
// ----------------------------------------------
// ----------------------------------------------
@implementation WBPageInfoModel

@end

// -----------------------视频信息-----------------------
// ----------------------------------------------
// ----------------------------------------------
@implementation WBPageMediaInfo

@end
