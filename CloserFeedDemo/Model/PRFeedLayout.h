//
//  PRFeedLayout.h
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PRFeedDataModel.h"

typedef NS_ENUM(NSInteger,WBCardType) {
    WBCardTypeArtical,
    WBCardTypePic,
    WBCardTypeVideo
};

@interface PRFeedLayout : NSObject

// -----------------------公开的方法-----------------------
+ (instancetype)WBLayoutWithStatusModel:(WBStatusModel *)statusModel;

- (void)updateDate;

// -----------------------微博的数据-----------------------
@property (nonatomic, strong) WBStatusModel *statusModel;


// -----------------------cell的高度-----------------------
@property (nonatomic, assign) CGFloat cellHieight;

// 图片的size
@property (nonatomic, assign) CGSize picSize;
// 播放按钮
@property (nonatomic, assign) CGRect playBtnF;
//播放时间
@property (nonatomic, assign) CGRect videoTimeF;
//artical文字
@property (nonatomic, assign) CGRect articalTitleF;
//artical封面
@property (nonatomic, assign) CGRect articalCoverF;
// 卡片内容类型
@property (nonatomic, assign) WBCardType contentCardType;

// -----------------------头部-----------------------
// 头像
@property (nonatomic, assign) CGRect headIconF;
// 昵称
@property (nonatomic, assign) CGRect nickNameF;
// 项目来源
@property (nonatomic, assign) CGRect timeSourceF;
//栏目头条
@property (nonatomic, assign) CGRect columnHeaderF;
// 头部容器
@property (nonatomic, assign) CGRect headContainerF;



// -----------------------正文-----------------------
// 正文的文字
@property (nonatomic, assign) CGRect contentLabelF;
// 正文卡片
@property (nonatomic, assign) CGRect cardContainerF;
// 正文容器
@property (nonatomic, assign) CGRect contentContainerF;



// -----------------------底部-----------------------
//分割线
@property (nonatomic, assign) CGRect sepLineF;
//底部容器
@property (nonatomic, assign) CGRect footConrainerF;

@end
