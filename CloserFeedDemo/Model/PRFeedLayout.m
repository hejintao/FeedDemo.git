//
//  PRFeedLayout.m
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import "PRFeedLayout.h"
#import "PRStatusHelper.h"

@interface PRFeedLayout()

@property (nonatomic, assign) CGFloat cardHeight;
@property (nonatomic, assign) CGFloat reweetCardHeight;

@end

@implementation PRFeedLayout

+ (instancetype)WBLayoutWithStatusModel:(WBStatusModel *)statusModel {
    PRFeedLayout *statusLayout = [[PRFeedLayout alloc]init];
    statusLayout.statusModel = statusModel;
    [statusLayout layout];
    return statusLayout;
}

- (void)updateDate {
    [self p_layoutHeader];
}

#pragma mark - pravite methods
- (void)layout {
    [self p_dealRichText];
    [self p_layoutHeader];
    [self p_layoutContent];
}

- (void)p_dealRichText {

    self.statusModel.richContent = [PRStatusHelper configWordAlignment:NSTextAlignmentLeft color:RGB(75, 73, 69) font:TextFont(17) str:self.statusModel.text];
    self.statusModel.richSouce =    [NSString stringWithFormat:@"%@   %@",self.statusModel.created_at,self.statusModel.source];
    
}

- (void)p_layoutHeader {
    self.columnHeaderF = CGRectMake(20, 0, 72, 35);
    self.headContainerF = CGRectMake(0, 0, SCREEN_WIDTH, self.statusModel.needShowHeaderContent ? 35 : 0);
}

-(void)p_layoutFooter{
    // 头像
    self.headIconF = CGRectMake(KCellPadding, 10, 17, 17);
    // 昵称
    CGFloat KNickNameX = CGRectGetMaxX(self.headIconF) + 8;
    CGFloat KNickNameHeight = [PRStatusHelper oneLineTextHeightWithFont:KCellSourceFont];
    self.nickNameF = CGRectMake(KNickNameX, (17 - KNickNameHeight) / 2.0 + 10, [PRStatusHelper oneLineTextWidthWithFont:KCellSourceFont text:self.statusModel.user.screen_name], KNickNameHeight);
    //后者和前者center_y对其  需要满足  后者的 y后 = (h前 - h后) / 2 + y前
    
    NSString *temp = @"5评论·6赞 6小时";
    CGFloat textWidth = [PRStatusHelper oneLineTextWidthWithFont:KCellSourceFont text:temp];
    
    self.timeSourceF = CGRectMake(kScreenWidth - 20 - textWidth, (17 - [PRStatusHelper oneLineTextHeightWithFont:KCellSourceFont]) / 2.0 + 10, textWidth, [PRStatusHelper oneLineTextHeightWithFont:KCellSourceFont]);
    
    self.sepLineF = CGRectMake(0, 40, kScreenWidth, 10);
    // 底部容器
    self.footConrainerF = CGRectMake(0, CGRectGetMaxY(self.contentContainerF) + 10, SCREEN_WIDTH, 50);
    
}

- (void)p_layoutContent {
    
    // 卡片类型的计算
    [self p_caluatePicSizeWithStatus:self.statusModel contentHeight:self.cardHeight];
    
    // 文字
    CGFloat contetLabelH = [PRStatusHelper heightForAttributeText:self.statusModel.richContent maxWidth:KCellContentWidth];
    self.contentLabelF = CGRectMake(KCellPadding, (self.contentCardType == WBCardTypeArtical) ? CGFLOAT_MIN : KCellPaddingText, KCellContentWidth, (self.contentCardType == WBCardTypeArtical) ? CGFLOAT_MIN : contetLabelH);
    
    // 卡片容器
    CGFloat cardContainerY = CGRectGetMaxY(self.contentLabelF) + KCellPaddingText;
    self.cardContainerF = CGRectMake(KCellPadding, cardContainerY, KCellContentWidth, self.cardHeight);
    
    // 卡片加上内容
    CGFloat contentContainerH = CGRectGetMaxY(self.cardContainerF);
    CGFloat contentContainerY = KCellPaddingText + CGRectGetMaxY(self.headContainerF);
    self.contentContainerF = CGRectMake( 0, contentContainerY, SCREEN_WIDTH, contentContainerH);
    
    [self p_layoutFooter];

    self.cellHieight = CGRectGetMaxY(self.contentContainerF) + 60.5;
    
}

- (void)p_caluatePicSizeWithStatus:(WBStatusModel *)statusModel contentHeight:(CGFloat)contentHeight{
    
    // 图片类型
    CGSize picSize = CGSizeZero;
    CGFloat cardContentHeight = 0;
    
    if (statusModel.picModelArray.count > 0) {// 有图片的情况下
        self.contentCardType = WBCardTypePic;
        
        CGFloat imgheight = [PRStatusHelper oneLineImageHeight:statusModel.picModelArray];
        
        picSize = CGSizeMake(imgheight, statusModel.picModelArray.count == 1 ? 424 * imgheight / 635.0 : imgheight);
        cardContentHeight = [PRStatusHelper heightForImages:statusModel.picModelArray];
    }
    
    
    if ([statusModel.page_info isKindOfClass:[WBPageInfoModel class]]) {
        // 视频
        if (statusModel.page_info.type == 11) {

            self.contentCardType = WBCardTypeVideo;
            
            picSize = CGSizeMake(SCREEN_WIDTH - 2 * KCellPadding, 0.5 * (SCREEN_WIDTH - 2 * KCellPadding));
            cardContentHeight = picSize.height;
            
            self.playBtnF = CGRectMake(picSize.width / 2 - 25, picSize.height / 2 - 25, 50, 50);
            self.videoTimeF = CGRectMake(picSize.width - 80 - 10, picSize.height - 25 - 10, 80, 25);
            
        }
        else{
            //文章;
            self.contentCardType = WBCardTypeArtical;
            NSMutableAttributedString *mStr = [PRStatusHelper configWordAlignment:NSTextAlignmentLeft color:RGB(75, 73, 69) font:TextFont(17) str:self.statusModel.page_info.page_title];
            CGFloat contetLabelH = [PRStatusHelper heightForAttributeText:mStr maxWidth:kScreenWidth - 20 - 13 -  114 - 20];
            
            self.articalTitleF = CGRectMake(0, 0, kScreenWidth - 20 - 13 -  114 - 20, contetLabelH);
            self.articalCoverF = CGRectMake(CGRectGetMaxX(self.articalTitleF) + 13, 0, 114, 75);
            
            cardContentHeight = MAX(contetLabelH, 75);
        }
    }
    
    self.picSize = picSize;
    self.cardHeight = cardContentHeight;
    
}

#pragma mark - setters
- (void)setStatusModel:(WBStatusModel *)statusModel {
    _statusModel = statusModel;
}

@end
