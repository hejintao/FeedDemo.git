//
//  PRTimeLineCardNode.h
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@class PRTImeLineCardVideoNode, PRTImeLineCardArticalNode;

@interface PRTimeLineCardNode : ASDisplayNode

@property (nonatomic, strong) PRFeedLayout *statusLayout;
@property (nonatomic, strong) PRTImeLineCardVideoNode *videoNode;
@property (nonatomic, strong) PRTImeLineCardArticalNode *articalNode;

@end

// -----------------------video类型-----------------------
@interface PRTImeLineCardVideoNode : ASDisplayNode

@property (nonatomic, strong) PRFeedLayout *statusLayout;
@property (nonatomic, copy) void(^prTimeLineCardVideoBlock)(void);

@end


// -----------------------artical类型-----------------------
@interface PRTImeLineCardArticalNode : ASDisplayNode

@property (nonatomic, strong) PRFeedLayout *statusLayout;

@end
