//
//  PRTimeLineCardNode.m
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import "PRTimeLineCardNode.h"

@interface PRTimeLineCardNode()

@property (nonatomic, strong) NSMutableArray *imageViewArray;
@property (nonatomic, assign) WBCardType cardType;

@end


@implementation PRTimeLineCardNode
#pragma mark - life cycle

- (instancetype)init {
    if (self = [super init]) {
        // video
        [self addSubnode:self.videoNode];
        // 图片
        for (int index = 0; index < 9; index++) {
            @autoreleasepool {
                ASNetworkImageNode *picImageNode = [[ASNetworkImageNode alloc]initWithWebImage];
                picImageNode.contentMode = UIViewContentModeScaleAspectFill;
                picImageNode.layer.masksToBounds = YES;
                picImageNode.hidden = YES;
                [self.imageViewArray addObject:picImageNode];
                [self addSubnode:picImageNode];
            }
        }
        //文章
        [self addSubnode:self.articalNode];
    }
    return self;
}

- (void)p_layoutCardWithstatusModel:(WBStatusModel *)statusModel picSize:(CGSize)picSize{
    self.cardType = _statusLayout.contentCardType;
    switch (self.cardType) {

        case WBCardTypePic:
        {
            NSInteger picCount = statusModel.picModelArray.count;
            for (int index = 0; index < picCount; index++) {
                @autoreleasepool {
                    ASNetworkImageNode *imageNode = self.imageViewArray[index];
                    if (index < picCount) {
                        // 位置赋值
                        imageNode.hidden = NO;
                        CGFloat x = (index % 3) * (KCellPicPadding + picSize.width);
                        CGFloat y = (index / 3) * (KCellPicPadding + picSize.height);
                        imageNode.frame = CGRectMake(x, y, picSize.width, picSize.height);
                        // 图片赋值
                        WBPicModel *picModel = statusModel.picModelArray[index];
                        imageNode.URL = [NSURL URLWithString:picModel.large.url];
                        
                    }
                }
            }
        }
            break;
        case WBCardTypeVideo:
        {
            self.videoNode.frame = CGRectMake(0, 0, picSize.width, picSize.height);
            self.videoNode.statusLayout = self.statusLayout;
        }
            break;
        case WBCardTypeArtical:
        {
            self.articalNode.frame = CGRectMake(0, 0, kScreenWidth - 2 * 20, MAX(CGRectGetHeight(self.statusLayout.articalTitleF), CGRectGetHeight(self.statusLayout.articalCoverF)));
            self.articalNode.statusLayout = self.statusLayout;
        }
            break;
        default:
            break;
    }
}


#pragma mark -setters
- (void)setStatusLayout:(PRFeedLayout *)statusLayout {
    _statusLayout = statusLayout;
    WBStatusModel *statusModel = statusLayout.statusModel;
    CGSize picSize = statusLayout.picSize;
    [self p_layoutCardWithstatusModel:statusModel picSize:picSize];
}
#pragma mark - setters
- (void)setCardType:(WBCardType)cardType {
    _cardType = cardType;
    switch (cardType) {
        case WBCardTypePic:
        {
            self.videoNode.hidden = YES;
            self.articalNode.hidden = YES;
        }
            break;

        case WBCardTypeVideo:
        {
            self.videoNode.hidden = NO;
            self.articalNode.hidden = YES;
            for (ASNetworkImageNode *itemNode in self.imageViewArray) {
                itemNode.hidden = YES;
            }
        }
            break;
        case WBCardTypeArtical:
        {
            self.videoNode.hidden = YES;
            for (ASNetworkImageNode *itemNode in self.imageViewArray) {
                itemNode.hidden = YES;
            }
            self.articalNode.hidden = NO;
        }
            break;
        default:
            break;
    }
}

#pragma mark - getters
- (NSMutableArray *)imageViewArray {
    if (_imageViewArray == nil) {
        _imageViewArray = [NSMutableArray array];
    }
    return _imageViewArray;
}

- (PRTImeLineCardVideoNode *)videoNode {
    if (_videoNode == nil) {
        _videoNode = [[PRTImeLineCardVideoNode alloc]init];
    }
    return _videoNode;
}

-(PRTImeLineCardArticalNode *)articalNode{
    if (_articalNode == nil) {
        _articalNode = [[PRTImeLineCardArticalNode alloc] init];
    }
    return _articalNode;
}

@end

// -----------------------video类型-----------------------
@interface PRTImeLineCardVideoNode()

@property (nonatomic, strong) ASNetworkImageNode *contentImageNode;
@property (nonatomic, strong) ASImageNode *playVideoNode;
@property (nonatomic, strong) ASTextNode *playTimeTextNode;

@end

@implementation PRTImeLineCardVideoNode
- (instancetype)init {
    if (self = [super init]) {
        
        [self addSubnode:self.contentImageNode];
        [self addSubnode:self.playVideoNode];
        [self addSubnode:self.playTimeTextNode];
        
    }
    return self;
}

- (void)layout {
    [super layout];
    
    self.contentImageNode.frame = CGRectMake(0, 0, self.calculatedSize.width, self.calculatedSize.height);
}

#pragma mark - setters
- (void)setStatusLayout:(PRFeedLayout *)statusLayout {
    _statusLayout = statusLayout;
    
    self.playVideoNode.frame = self.statusLayout.playBtnF;
    self.playTimeTextNode.frame = self.statusLayout.videoTimeF;
    
    self.contentImageNode.URL = [NSURL URLWithString:statusLayout.statusModel.page_info.page_pic];
    
    YYLabel *label = (YYLabel *)self.playTimeTextNode.view;
    
    label.attributedText = [PRStatusHelper configWordAlignment:NSTextAlignmentCenter color:[UIColor whiteColor] font:TextFont(12) str:@"0:00/0:44"];
    
}

- (void)setNormalMode{
    self.playVideoNode.hidden = YES;
}

-(void)playVideoAction{
    
    [self setNormalMode];
    
    if (_prTimeLineCardVideoBlock) {
        _prTimeLineCardVideoBlock();
    }
}

#pragma mark - getters
- (ASNetworkImageNode *)contentImageNode {
    if (_contentImageNode == nil) {
        _contentImageNode = [[ASNetworkImageNode alloc]initWithWebImage];
        _contentImageNode.view.tag = 10001;
    }
    return _contentImageNode;
}

- (ASImageNode *)playVideoNode {
    if (_playVideoNode == nil) {
        _playVideoNode = [[ASImageNode alloc] init];
        _playVideoNode.image = [UIImage imageNamed:@"btn_video_pause"];
        [_playVideoNode addTarget:self action:@selector(playVideoAction) forControlEvents:ASControlNodeEventTouchUpInside];
    }
    return _playVideoNode;
}

- (ASTextNode *)playTimeTextNode {
    if (_playTimeTextNode == nil) {
        
        _playTimeTextNode = [[ASTextNode alloc] initWithViewBlock:^UIView * _Nonnull{
            YYLabel *label = [[YYLabel alloc]init];
            label.layer.cornerRadius = 12.5;
            return label;
        }];
        _playTimeTextNode.backgroundColor = RGB_A(0, 0, 0, 0.3);
        
    }
    return _playTimeTextNode;
}


@end

// -----------------------artical类型-----------------------
@interface PRTImeLineCardArticalNode()

@property (nonatomic, strong) ASTextNode *titleNode;
@property (nonatomic, strong) ASNetworkImageNode *coverNode;

@end

@implementation PRTImeLineCardArticalNode

- (instancetype)init {
    if (self = [super init]) {
        
        [self addSubnode:self.titleNode];
        [self addSubnode:self.coverNode];
        
    }
    return self;
}

-(void)setStatusLayout:(PRFeedLayout *)statusLayout{
    _statusLayout = statusLayout;
    
    self.titleNode.frame = self.statusLayout.articalTitleF;
    self.coverNode.frame = self.statusLayout.articalCoverF;
    
    self.coverNode.URL = [NSURL URLWithString:statusLayout.statusModel.page_info.page_pic];

    YYLabel *label = (YYLabel *)self.titleNode.view;

    label.attributedText = [PRStatusHelper configWordAlignment:NSTextAlignmentLeft color:RGB(75, 73, 69) font:TextFont(17) str:[NSString stringWithFormat:@"%@",statusLayout.statusModel.page_info.page_title]];
}

#pragma mark - getters
- (ASNetworkImageNode *)coverNode {
    if (_coverNode == nil) {
        _coverNode = [[ASNetworkImageNode alloc]initWithWebImage];
    }
    return _coverNode;
}

- (ASTextNode *)titleNode {
    if (_titleNode == nil) {
        
        _titleNode = [[ASTextNode alloc] initWithViewBlock:^UIView * _Nonnull{
            YYLabel *label = [[YYLabel alloc]init];
            label.displaysAsynchronously = YES;
            label.numberOfLines = 3;
            return label;
        }];
        
    }
    return _titleNode;
}

@end
