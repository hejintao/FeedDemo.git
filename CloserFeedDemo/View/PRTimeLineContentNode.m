//
//  PRTimeLineContentNode.m
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import "PRTimeLineContentNode.h"
#import "PRTimeLineCardNode.h"
@interface PRTimeLineContentNode()

@property (nonatomic, strong) ASDisplayNode *contentTextNode;
@property (nonatomic, strong) PRTimeLineCardNode *cardNode;

@end

@implementation PRTimeLineContentNode
- (instancetype)init {
    if (self = [super init]) {
        [self addSubnode:self.contentTextNode];
        [self addSubnode:self.cardNode];
    }
    return self;
}

#pragma mark - setters
- (void)setStatusLayout:(PRFeedLayout *)statusLayout {
    _statusLayout = statusLayout;
    // 位置
    self.contentTextNode.frame = statusLayout.contentLabelF;
    self.cardNode.frame = statusLayout.cardContainerF;
    // 数据
    YYLabel *label = (YYLabel *)self.contentTextNode.view;
    label.attributedText = statusLayout.statusModel.richContent;

    NSString *string = @"...全文";
    NSMutableAttributedString *truncation = [[NSMutableAttributedString alloc] initWithString:string];
    [truncation addAttribute:NSFontAttributeName
                       value:TextFont(17)
                       range:string.rangeOfAll];
    [truncation addAttribute:NSForegroundColorAttributeName
                       value:UIColorHex(507CAF)
                       range:NSMakeRange(string.length-2, 2)];
    label.truncationToken = truncation;
    
    self.cardNode.statusLayout = statusLayout;
    self.backgroundColor = [UIColor whiteColor];
}

#pragma mark - getters
- (ASDisplayNode *)contentTextNode {
    if (_contentTextNode == nil) {
        _contentTextNode = [[ASDisplayNode alloc]initWithViewBlock:^UIView * _Nonnull{
            YYLabel *label = [[YYLabel alloc]init];
            label.numberOfLines = 7;
            label.font = TextFont(17);
            label.textColor = RGB(75, 73, 69);
            label.displaysAsynchronously = YES;
            return label;
        }];
    }
    return _contentTextNode;
}


- (PRTimeLineCardNode *)cardNode {
    if (_cardNode == nil) {
        _cardNode = [[PRTimeLineCardNode alloc]init];
        W_S
        _cardNode.videoNode.prTimeLineCardVideoBlock = ^{
                        
            if (weakSelf.prtimeLineCOntentBlock) {
                weakSelf.prtimeLineCOntentBlock();
            }
        };
    }
    return _cardNode;
}
@end
