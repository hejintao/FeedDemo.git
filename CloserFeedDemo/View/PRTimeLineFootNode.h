//
//  PRTimeLineFootNode.h
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>

@interface PRTimeLineFootNode : ASDisplayNode
@property (nonatomic, strong) PRFeedLayout *statusLayout;
@end
