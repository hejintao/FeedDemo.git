//
//  PRTimeLineFootNode.m
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import "PRTimeLineFootNode.h"

@interface PRTimeLineFootNode()

@property (nonatomic, strong) ASDisplayNode *toolBarSepLineNode;

@property (nonatomic, strong) ASTextNode *nickNameTextNode;
@property (nonatomic, strong) ASTextNode *timeSouceTextNode;
@property (nonatomic, strong) ASNetworkImageNode *headIconNode;

@end

@implementation PRTimeLineFootNode

#pragma mark - life cycle
- (instancetype)init {
    if (self = [super init]) {
        [self addSubnode:self.toolBarSepLineNode];
        
        [self addSubnode:self.nickNameTextNode];
        [self addSubnode:self.timeSouceTextNode];
        [self addSubnode:self.headIconNode];
    }
    return self;
}

#pragma mark - setters
- (void)setStatusLayout:(PRFeedLayout *)statusLayout {
    
    _statusLayout = statusLayout;
    // 设置位置
    self.headIconNode.frame = self.statusLayout.headIconF;
    self.nickNameTextNode.frame = self.statusLayout.nickNameF;
    self.timeSouceTextNode.frame = self.statusLayout.timeSourceF;
    self.toolBarSepLineNode.frame = self.statusLayout.sepLineF;
    // 设置数据
    self.headIconNode.URL = [NSURL URLWithString:self.statusLayout.statusModel.user.profile_image_url];
    self.nickNameTextNode.attributedText = [PRStatusHelper attributedStringWithText:self.statusLayout.statusModel.user.screen_name textFont:KCellSourceFont textColor:RGB(148, 146, 142)];
    self.timeSouceTextNode.attributedText = [PRStatusHelper attributedStringWithText:@"5评论·6赞 6小时" textFont:KCellSourceFont textColor:RGB(148, 146, 142)];
    
}

#pragma mark - getters

-(ASDisplayNode *)toolBarSepLineNode{
    if (!_toolBarSepLineNode) {
        _toolBarSepLineNode = [PRAsKitMaster nodeDisplayNodeAddNode:self BackgroundColor:RGB(245, 245, 245)];
    }
    return _toolBarSepLineNode;
}

- (ASTextNode *)nickNameTextNode {
    if (_nickNameTextNode == nil) {
        _nickNameTextNode = [[ASTextNode alloc]init];
    }
    return _nickNameTextNode;
}

- (ASTextNode *)timeSouceTextNode {
    if (_timeSouceTextNode == nil) {
        _timeSouceTextNode = [[ASTextNode alloc]init];
    }
    return _timeSouceTextNode;
}

- (ASNetworkImageNode *)headIconNode {
    if (_headIconNode == nil) {
        _headIconNode = [[ASNetworkImageNode alloc]initWithWebImage];
        
        ///此处为设置圆角
        _headIconNode.imageModificationBlock = ^UIImage *(UIImage *image) {
            
            UIImage *modifiedImage;
            CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
            
            UIGraphicsBeginImageContextWithOptions(image.size, false, [[UIScreen mainScreen] scale]);
            [[UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(5, 5)] addClip];
            [image drawInRect:rect];
            modifiedImage = UIGraphicsGetImageFromCurrentImageContext();
            
            UIGraphicsEndImageContext();
            
            return modifiedImage;
            
        };
    }
    return _headIconNode;
}

@end
