//
//  PRTimeLineHardCell.h
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "PRFeedLayout.h"

@protocol PRTimeLineHardCellDelegate <NSObject>
@optional

-(void)prTimeLineHardCell_playTheVideoAtIndexPath:(NSIndexPath *)index;

@end

@interface PRTimeLineHardCell : ASCellNode

@property (nonatomic, strong) PRFeedLayout *statusLayout;

- (void)setDelegate:(id<PRTimeLineHardCellDelegate>)delegate withIndexPath:(NSIndexPath *)index_path;

@end

