//
//  PRTimeLineHardCell.m
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import "PRTimeLineHardCell.h"
#import "PRTimeLineContentNode.h"
#import "PRTimeLineHeadNode.h"
#import "PRTimeLineCardNode.h"
#import "PRTimeLineFootNode.h"

@interface PRTimeLineHardCell()
@property (nonatomic, strong) PRTimeLineHeadNode *headerNode;
@property (nonatomic, strong) PRTimeLineContentNode *contentNode;
@property (nonatomic, strong) PRTimeLineFootNode *footerNode;

@property (nonatomic, weak) id<PRTimeLineHardCellDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *index_path;

@end

@implementation PRTimeLineHardCell
#pragma mark - life cycle
- (instancetype)init {
    if (self = [super init]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addSubnode:self.headerNode];
        [self addSubnode:self.contentNode];
        [self addSubnode:self.footerNode];
        
    }
    return self;
}

- (CGSize)calculateSizeThatFits:(CGSize)constrainedSize {
    return CGSizeMake(constrainedSize.width, self.statusLayout.cellHieight);
}

#pragma mark - setters
- (void)setStatusLayout:(PRFeedLayout *)statusLayout {
    _statusLayout = statusLayout;
    // 头部
    self.headerNode.frame = statusLayout.headContainerF;
    self.headerNode.statusLayout = statusLayout;
    // 内容
    self.contentNode.frame = statusLayout.contentContainerF;
    self.contentNode.statusLayout = statusLayout;
    //底部
    self.footerNode.frame = statusLayout.footConrainerF;
    self.footerNode.statusLayout = statusLayout;

}

-(void)setDelegate:(id<PRTimeLineHardCellDelegate>)delegate withIndexPath:(NSIndexPath *)indexPath{
    self.delegate = delegate;
    self.index_path = indexPath;
}

#pragma mark - getters
- (PRTimeLineHeadNode *)headerNode {
    if (_headerNode == nil) {
        _headerNode = [[PRTimeLineHeadNode alloc]init];
    }
    return _headerNode;
}

- (PRTimeLineContentNode *)contentNode {
    if (_contentNode == nil) {
        _contentNode = [[PRTimeLineContentNode alloc]init];
        W_S
        _contentNode.prtimeLineCOntentBlock = ^{
            if ([weakSelf.delegate respondsToSelector:@selector(prTimeLineHardCell_playTheVideoAtIndexPath:)]) {
                [weakSelf.delegate prTimeLineHardCell_playTheVideoAtIndexPath:weakSelf.indexPath];
            }
        };
    }
    return _contentNode;
}

-(PRTimeLineFootNode *)footerNode{
    if (_footerNode == nil) {
        _footerNode = [[PRTimeLineFootNode alloc] init];
    }
    return _footerNode;
}

@end
