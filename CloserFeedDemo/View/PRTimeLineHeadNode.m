//
//  PRTimeLineHeadNode.m
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import "PRTimeLineHeadNode.h"

@interface PRTimeLineHeadNode()
@property (nonatomic, strong) ASImageNode *columnHeaderImgV;
@end

@implementation PRTimeLineHeadNode

#pragma mark - life cycle
- (instancetype)init {
    if (self = [super init]) {
        
        [self addSubnode:self.columnHeaderImgV];
    }
    return self;
}

#pragma mark - setters
- (void)setStatusLayout:(PRFeedLayout *)statusLayout {
    _statusLayout = statusLayout;
    
    if (_statusLayout.statusModel.needShowHeaderContent) {
        self.columnHeaderImgV.frame = self.statusLayout.columnHeaderF;
        self.columnHeaderImgV.image = [UIImage imageNamed:@"column_header_icon"];
    }
    
}

#pragma mark - getters

-(ASImageNode *)columnHeaderImgV{
    if (!_columnHeaderImgV) {
        _columnHeaderImgV = [[ASImageNode alloc] init];
    }
    return _columnHeaderImgV;
}

@end
