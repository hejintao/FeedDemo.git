//
//  ViewController.m
//  CloserFeedDemo
//
//  Created by 何锦涛 on 2018/7/2.
//  Copyright © 2018年 hither. All rights reserved.
//

#import "ViewController.h"
#import "PRTimeLineHardCell.h"
#import "YYFPSLabel.h"
#import "ZFPlayer.h"
#import "ZFAVPlayerManager.h"
#import "ZFPlayerControlView.h"
#import "DetailViewController.h"


@interface ViewController ()<ASTableDelegate,ASTableDataSource,PRTimeLineHardCellDelegate>

@property (nonatomic, strong) ASTableNode *tableNode;
@property (nonatomic, strong) NSMutableArray <PRFeedLayout *> *layoutArray;

@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) ZFPlayerControlView *controlView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubnode:self.tableNode];
    
//    UILabel *node = [[UIView alloc] init];
//    node.frame = CGRectMake(0, 0, kScreenWidth, 200);
//    node.backgroundColor = RGB(244, 244, 244);
//
//    self.tableNode.view.tableHeaderView = node;
    
    AdjustsScrollViewInsetNever(self , self.tableNode.view);
    
    [self p_loadDatas];
    
    ZFAVPlayerManager *playerManager = [[ZFAVPlayerManager alloc] init];
    self.player = [ZFPlayerController playerWithScrollView:self.tableNode.view playerManager:playerManager containerViewTag:10001];
    self.player.controlView = self.controlView;
    
    /// 0.8是消失80%时候，默认0.5
    self.player.playerDisapperaPercent = 0.8;
    /// 移动网络依然自动播放
    self.player.WWANAutoPlay = YES;

    [self showFpsInfo];
    
}

- (void)showFpsInfo{
    YYFPSLabel *fpsLabel = [[YYFPSLabel alloc] initWithFrame:CGRectMake(20, 30, 60, 30)];
    [fpsLabel sizeToFit];
    [self.navigationController.view addSubview:fpsLabel];
}

#pragma mark - pravite methods
- (void)p_loadDatas {
    
    NSMutableArray *ary = [NSMutableArray array];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (int index = 0; index < 7; index++) {
            @autoreleasepool {
                NSString *soureName = [NSString stringWithFormat:@"weibo_%d.json",index];
                NSData *weiboData = [PRStatusHelper WBDataWithSourceName:soureName];
                
                PRFeedDataModel *timeLineModel = [PRFeedDataModel modelWithJSON:weiboData];
                
                [timeLineModel.statuses enumerateObjectsUsingBlock:^(WBStatusModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    [ary addObject:[NSURL URLWithString:@"http:\/\/tb-video.bdstatic.com\/tieba-smallvideo-transcode\/3612804_e50cb68f52adb3c4c3f6135c0edcc7b0_3.mp4"]];
                    self.player.assetURLs = ary.copy;
                    
                    if ((obj.picModelArray.count > 0) || (obj.page_info.type == 1) || (obj.page_info.page_title.length > 0) || (obj.page_info.page_pic.length > 0)) {
                        
                        PRFeedLayout *layout = [PRFeedLayout WBLayoutWithStatusModel:obj];
                        
                        [self.layoutArray addObject:layout];
                    }
                    
                }];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableNode reloadData];
        });
    });
}

#pragma mark - delegates
- (NSInteger)numberOfSectionsInTableNode:(ASTableNode *)tableNode {
    return 1;
}

- (NSInteger)tableNode:(ASTableNode *)tableNode numberOfRowsInSection:(NSInteger)section {
    return self.layoutArray.count;
}

- (ASCellNode *)tableNode:(ASTableNode *)tableNode nodeForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PRTimeLineHardCell *hardNode = [[PRTimeLineHardCell alloc]init];
    hardNode.statusLayout = self.layoutArray[indexPath.row];
    
    [hardNode setDelegate:self withIndexPath:indexPath];
    
    return hardNode;
}

-(void)tableNode:(ASTableNode *)tableNode didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailViewController *vc = [DetailViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)prTimeLineHardCell_playTheVideoAtIndexPath:(NSIndexPath *)index{
    [self playTheVideoAtIndexPath:index scrollToTop:NO];
}

/// play the video
- (void)playTheVideoAtIndexPath:(NSIndexPath *)indexPath scrollToTop:(BOOL)scrollToTop {
    [self.player playTheIndexPath:indexPath scrollToTop:scrollToTop];

    [self.controlView showTitle:@""
                 coverURLString:@""
                 fullScreenMode:ZFFullScreenModePortrait];
}

#pragma mark - getters
- (ASTableNode *)tableNode {
    if (_tableNode == nil) {
        _tableNode = [[ASTableNode alloc]initWithStyle:UITableViewStylePlain];
        _tableNode.frame = CGRectMake(0, 64, kScreenWidth, SCREEN_HEIGHT-64);
        _tableNode.delegate = self;
        _tableNode.dataSource = self;
        _tableNode.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableNode.view.separatorStyle = UITableViewCellSeparatorStyleNone;
        
//        @weakify(self)
//        _tableNode.view.zf_scrollViewDidStopScrollCallback = ^(NSIndexPath * _Nonnull indexPath) {
//            @strongify(self)
//            [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
//        };
    }
    return _tableNode;
}

- (NSMutableArray<PRFeedLayout *> *)layoutArray {
    if (_layoutArray == nil) {
        _layoutArray = [NSMutableArray array];
    }
    return _layoutArray;
}

- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
    }
    return _controlView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
